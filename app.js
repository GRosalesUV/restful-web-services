const express = require('express');
const mongoose = require('mongoose');
const debug = require('debug')('app');
const bookRouter = require('./routes/bookRouter');

const app = express();
if (process.env.ENV === 'Test') {
  console.log('This is a test');
  const db = mongoose.connect('mongodb://root:123456@mongo:27017/bookAPI_Test?authSource=admin', { useNewUrlParser: true, useUnifiedTopology: true });
} else {
  const db = mongoose.connect('mongodb://root:123456@mongo:27017/bookAPI?authSource=admin', { useNewUrlParser: true, useUnifiedTopology: true });
}
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', bookRouter);
app.get('/', async (req, res) => {
  res.status(200).send('Hola Mundo!');
});

app.server = app.listen(port, () => debug(`Started on ${port} port`));

module.exports = app;
