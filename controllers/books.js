const debug = require('debug')('app:bookController');
const BookModel = require('../models/Book');

const booksController = (Book = BookModel) => {
  const middleware = async (req, res, next) => {
    try {
      const book = await Book.findById(req.params.id).exec();

      debug(book);

      if (!book) {
        return res.sendStatus(404);
      }

      req.book = book;
      next();
    } catch (e) {
      debug(e);
      return res.sendStatus(404);
    }
  };

  const getAll = async (req, res) => {
    const query = {};
    if (req.query.genre) {
      query.genre = req.query.genre;
    }

    debug(query);

    const rawBooks = await Book.find(query);
    const books = rawBooks.map((book) => {
      const newBook = book.toJSON();
      newBook.links = {};
      newBook.links.self = `http://${req.headers.host}/api/books/${book._id}`;
      return newBook;
    });

    res.json(books);
  };

  const create = async (req, res) => {
    if (!req.body.book.title) {
      res.status(400);
      return res.send('Title is required');
    }

    const book = new Book({ ...req.body.book });

    await book.save();

    debug(book);

    res.status(201);
    return res.json(book);
  };

  const getById = async (req, res) => {
    const { book } = req;
    const customBook = book.toJSON();

    customBook.links = {};
    customBook.links.FilterByThisGenre = `http://${req.headers.host}/api/books?genre=${book.genre.replace(' ', '%20')}`;

    debug(customBook);

    res.json(customBook);
  };

  const put = async (req, res) => {
    const { book } = req;

    debug(req.body);

    book.read = req.body.read;
    book.title = req.body.title;
    book.genre = req.body.genre;
    book.author = req.body.author;

    await book.save();
    return res.status(202).json(book);
  };

  const patch = async (req, res) => {
    const { book } = req;
    const bookParams = req.body.book;

    // eslint-disable-next-line no-underscore-dangle
    if (bookParams._id) {
      // eslint-disable-next-line no-underscore-dangle
      delete bookParams._id;
    }

    Object.entries(bookParams).forEach(([key, val]) => {
      book[key] = val;
    });

    debug(book);

    await book.save();
    return res.status(200).json(book);
  };

  const destroy = async (req, res) => {
    const book = await req.book.remove();

    debug(book);

    return res.sendStatus(204);
  };

  return {
    middleware,
    getAll,
    create,
    getById,
    put,
    patch,
    destroy
  };
};

module.exports = booksController;
