const should = require('should');
const sinon = require('sinon');
const bookController = require('../controllers/books');

describe('Book Controller test', () => {
  describe('Post', () => {
    it('should not allow an empty title on post', () => {
      const Book = function (book) { this.save = () => { } };
      const req = {
        body: {
          author: 'Gio'
        }
      };

      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy()
      };

      bookController(Book).create(req, res);

      console.log(res.status.args);
      res.status.calledWith(400).should.equal(true, `Bad Request`);
      res.send.calledWith('Title is required').should.equal(true);
    });
  });
});
