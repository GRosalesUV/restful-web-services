const express = require('express');
const booksController = require('../controllers/books')();

const routes = () => {
  const bookRouter = express.Router();

  bookRouter.route('/books')
    .get(booksController.getAll)
    .post(booksController.create);

  bookRouter.route('/books/:id')
    .all(booksController.middleware)
    .get(booksController.getById)
    .put(booksController.put)
    .patch(booksController.patch)
    .delete(booksController.destroy);

  return bookRouter;
};

module.exports = routes();
